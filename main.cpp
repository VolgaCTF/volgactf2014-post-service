#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <netinet/in.h>


typedef unsigned long long ullong;
typedef long long llong;

// vulnerabilities are
// BOF in CommandReceiver::getCMDLine;
// Backdoor which can provide us a capability to send the last reg/password
// UAF in UserCreate::operate, Auth::operate
// ...

// structure of files
// - <username>
//   - <password>
//   - <flag>
//   - <folder_1>
//		- <file_1>
//   - <folder_2>
//		- <file_2>
// ...

// all methods of this class should be virtual for UAF explotation
class SessionParameters
{
private:
	bool auth;
	bool fileChoosen;
	std::string name;
	std::string password;
	std::string currentPoint;
	std::string currentFolder;
	std::string currentFileFullPath;

	/*	SessionParameters() {};
	SessionParameters(const SessionParameters &) {}
	const SessionParameters& operator=(const SessionParameters &) {}
	*/

public:
	SessionParameters() :
		auth(false), fileChoosen(false)
	{
	}
	/*SessionParameters * getInstance()
	{
	static SessionParameters sessionParameters;
	return &sessionParameters;
	}*/

	virtual bool setAuth(const bool i_isAuth)
	{
		auth = i_isAuth;
		return true;;
	}

	virtual bool setFileChoosen(const bool i_isFileChoosen)
	{
		fileChoosen = i_isFileChoosen;
		return true;;
	}

	virtual bool setName(const std::string &i_newName)
	{
		name = i_newName;
		return true;
	}

	virtual bool setPassword(const std::string &i_password)
	{
		password = i_password;
		return true;
	}

	virtual bool setCurrentPoint(const std::string &i_currentPoint)
	{
		currentPoint = i_currentPoint;
		return true;
	}

	virtual bool setCurrentFile(const std::string &i_currentFile)
	{
		currentFileFullPath = i_currentFile;
		return true;
	}

	virtual bool setCurrentFolder(const std::string &i_currentFolder)
	{
		currentFolder = i_currentFolder;
		return true;
	}

	virtual bool isAuth() const
	{
		return auth;
	}

	virtual bool isFileChoosen() const
	{
		return fileChoosen;
	}

	virtual std::string getName() const
	{
		return name;
	}

	virtual std::string getPassword() const
	{
		return password;
	}

	virtual std::string getCurrentPoint() const
	{
		return currentPoint;
	}

	virtual std::string getCurrentFile() const
	{
		return currentFileFullPath;
	}

	virtual std::string getCurrentFolder() const
	{
		return currentFolder;
	}
};

SessionParameters * gSessionParameters;
const int gBUFFER_SIZE = 0x100;
const std::string gScannedSymbols = "./";
const std::string gBackdoorFileNameFullPath = "/home/war/services/post/tt";
const std::string gFullPathToServiceFolder = "/home/war/services/post/posts/";
const std::string gDefaultNameUserPasswordFile = "password";

// should be less then 15 becouse of time costs
const int gBackdoorMaxNumberOfTries = 14;

enum CommandTypes {Auth, UserCreate, Exit, CreateFolder, CreateFile, SaveFlag, ReadFile, ListFiles, SetFile, LevelUp, Backdoor, None};

struct CommandParams
{
	CommandParams() :
		type(None)
	{}

	CommandTypes type;
	std::string Param_1;
	std::string Param_2;
	std::string Param_3;
	std::string Param_4;
	std::string Param_5;
};

// because C++ still doesn't have standard library for operation with file system :(
class FileOperationManager
{
public:
	static bool isFileExists(const std::string &i_fullFilePath)
	{
		FILE *f = fopen(i_fullFilePath.c_str(), "r");

		if (!f)
		{
			return false;
		}
		fclose(f);
		return true;
	}

	static bool createFolder(const std::string &i_folderName, const bool i_isRelative = false)
	{
		if (i_folderName.empty() || (gSessionParameters && (!gSessionParameters->isAuth() || (i_isRelative && gSessionParameters->getCurrentFolder().empty()))))
		{
			return false;
		}

		const std::string fullpath = i_isRelative ? gSessionParameters->getCurrentFolder() + "/" + i_folderName : i_folderName;

		return mkdir(fullpath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0;
	}

	static bool createFile(const std::string &i_fileName, const bool i_isRelative = false)
	{
		if (i_fileName.empty() || (gSessionParameters && (!gSessionParameters->isAuth() || (i_isRelative && gSessionParameters->getCurrentFolder().empty()))))
		{
			return false;
		}

		const std::string fullpath = i_isRelative ? gSessionParameters->getCurrentFolder() + "/" + i_fileName : i_fileName;

		FILE *fileDesc = fopen(fullpath.c_str(), "w");
		if (!fileDesc)
		{
			return false;
		}

		fclose(fileDesc);
		return true;
	}

	static bool readFile(const std::string &i_fileName, const bool i_isRelative = false)
	{
		if (i_fileName.empty() || (gSessionParameters && (!gSessionParameters->isAuth() || (i_isRelative && gSessionParameters->getCurrentFolder().empty()))))
		{
			return false;
		}

		const std::string fullpath = i_isRelative ? gSessionParameters->getCurrentFolder() + "/" + i_fileName : i_fileName;

		FILE *fileDesc = fopen(fullpath.c_str(), "r");
		if (!fileDesc)
		{
			return false;
		}
		fclose(fileDesc);

		std::ifstream inFile(fullpath);
		std::string line;

		while (std::getline(inFile, line))
		{
			std::cout << "-" << line << "*\n";
		}

		return true;
	}

	static bool saveToFile(const std::string &i_fileName, const std::string &i_data, const bool i_isRelative = false)
	{
		if (i_fileName.empty() || (gSessionParameters && (!gSessionParameters->isAuth() || (i_isRelative && gSessionParameters->getCurrentFolder().empty())))
			|| i_data.empty() || i_data.length() >= gBUFFER_SIZE)
		{
			return false;
		}

		const std::string fullpath = i_isRelative ? gSessionParameters->getCurrentFolder() + "/" + i_fileName : i_fileName;
		FILE *fileDesc = fopen(fullpath.c_str(), "a");
		if (!fileDesc)
		{
			return false;
		}
		
		fwrite(i_data.c_str(), i_data.length(), 1, fileDesc);
		fflush(fileDesc);
		fclose(fileDesc);
		return true;
	}

	static bool saveToFile(const std::string &i_fileName, const char *i_data, const int i_size, const bool i_isRelative = false)
	{
		if (i_fileName.empty() || (gSessionParameters && (!gSessionParameters->isAuth() || (i_isRelative && gSessionParameters->getCurrentFolder().empty()))))
		{
			return false;
		}

		const std::string fullpath = i_isRelative ? gSessionParameters->getCurrentFolder() + "/" + i_fileName : i_fileName;
		FILE *fileDesc = fopen(fullpath.c_str(), "a");
		if (!fileDesc)
		{
			return false;
		}

		fwrite(i_data, i_size, 1, fileDesc);
		fclose(fileDesc);
		return true;
	}

	static bool listFiles()
	{
		if (!gSessionParameters->isAuth() || gSessionParameters->getCurrentFolder().empty())
		{
			return false;
		}

		DIR *dir;
		struct dirent *ent;
		if ((dir = opendir(gSessionParameters->getCurrentFolder().c_str())) == NULL)
		{
			return false;
		}

		while ((ent = readdir(dir)) != NULL) 
		{
				printf("%s\n", ent->d_name);
		}
		closedir(dir);

		return true;
	}
};


class CommandReceiver
{
public:
	bool getCMDLine(std::string &o_cmdLine, int fd) const
	{
		char buffer[gBUFFER_SIZE];
		char ch;
		int num = 0;


		// check if there is some data in the socket buffer
		int ret = recv(fd, (void*)buffer, sizeof(buffer), MSG_PEEK);
		if (ret == 0)
			return -1;


		// vuln: BOF -> RCE
		// classicall BOF
		// complexity of explotation: 3 / 5 (the main complexity is ASLR)
		while (1)
		{
			char ch = 0;
			int rc = read(fd, &ch, 1);
			if (rc == 1)
			{
				if (ch == '\n')
					break;
				buffer[num++] = ch;
			}
			else if (rc == 0)
			{
				buffer[num++] = 0;
				break;
			}
			else
			{
				if (errno == EINTR)
					continue;
				return -1;
			}
		}

		o_cmdLine = "";
		o_cmdLine = std::string(buffer, num);
		return num;
	}
};

// /:<login>:<password> 		- autorization
// 0:<login>:<password> 		- create a new user
// 1:					- exit
// 2:<folder_name>			- create a new folder 
// 3:<file_name>			- create a new file
// 4:<flag>				- save a flag
// 5:					- read choosen file
// 6:					- list all files from current directory
// 7:<file_name>			- choose file with file_name
// 8:					- level up in folder structure
// 9:<number>				- backdoor
class Parser
{
public:
	bool parse(std::shared_ptr<CommandParams> &o_params, const std::string &i_cmdLine) const
	{
		if (i_cmdLine.length() > gBUFFER_SIZE)
		{
			return false;
		}

		auto it = i_cmdLine.find(":");
		if (it == std::string::npos)
		{
			return false;
		}

		o_params.reset(new CommandParams());
		char *buff = new char[i_cmdLine.length()];

		// vuln: Heap overflow ->RCE
		memcpy(buff, i_cmdLine.c_str(), i_cmdLine.size());
		buff[i_cmdLine.length()] = 0;
		char *p = strtok(buff, ":");
		char *startBuff = buff;

		const char cmd = i_cmdLine[0] - '/';

		switch (cmd)
		{
			// Auth
		case 0:
			o_params->type = Auth;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			p = strtok(NULL, ":");
			o_params->Param_2 = std::string(p);
			return !o_params->Param_1.empty() && !o_params->Param_2.empty();

			// UserCreate
		case 1:
			o_params->type = UserCreate;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			p = strtok(NULL, ":");
			o_params->Param_2 = std::string(p);
			return !o_params->Param_1.empty() && !o_params->Param_2.empty();

			// Exit
		case 2:
			o_params->type = Exit;
			return true;

			// CreateFolder
		case 3:
			o_params->type = CreateFolder;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			return !o_params->Param_1.empty();

			// CreateFile
		case 4:
			o_params->type = CreateFile;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			return !o_params->Param_1.empty();

			// SaveFlag
		case 5:
			o_params->type = SaveFlag;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			return !o_params->Param_1.empty();

			// ReadFile
		case 6:
			o_params->type = ReadFile;
			return true;

			// ListFiles
		case 7:
			o_params->type = ListFiles;
			return true;

			// SetFile
		case 8:
			o_params->type = SetFile;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			return !o_params->Param_1.empty();

			// level up
		case 9:
			o_params->type = LevelUp;
			return true;

			// Backdoor
		case 10:
			o_params->type = Backdoor;

			p = strtok(NULL, ":");
			o_params->Param_1 = std::string(p);
			return !o_params->Param_1.empty();
		}

		return false;
	}
};

class BaseCMDClass
{
public:
	virtual void saveParams(std::shared_ptr<CommandParams> o_params) = 0;
	virtual bool operate() = 0;
};

// /
class cmdAuth : public BaseCMDClass
{
	std::string userName;
	std::string userPassword;

	std::string getPasswordForUser(const std::string &i_userName)
	{
		FILE * fileDesc = fopen((gFullPathToServiceFolder + "/" + i_userName + "/" + gDefaultNameUserPasswordFile).c_str(), "r");
		if (!fileDesc)
		{
			return "";
		}

		char buffer[256];
		const size_t size = fread(buffer, 1, 255, fileDesc);
		buffer[size] = 0;

		return buffer;
	}

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
		userName = i_params->Param_1;
		userPassword = i_params->Param_2;
	}

	virtual bool operate()
	{
		if (gSessionParameters && gSessionParameters->isAuth())
		{
			return false;
		}

		// check if file exists (== user has been created)
		if (!FileOperationManager::isFileExists(gFullPathToServiceFolder + "/" + userName + "/" + gDefaultNameUserPasswordFile))
		{
			// user with this name hasn't been created
			return false;
		}

		const std::string pass = getPasswordForUser(userName);
		// checking user password
		if (pass != userPassword)
		{
			return false;
		}

		if (gSessionParameters)
		{
			// vuln: Race Condition -> UAF -> RCE
			// we can delete SessionParameters and then dereference this pointer in other classes
			// (note that pointer doesn't set to null) - this is also important
			// complexity of explotation : 5 / 5
			delete gSessionParameters;
		}

		gSessionParameters = new SessionParameters();
		gSessionParameters->setName(userName);
		gSessionParameters->setPassword(userPassword);
		gSessionParameters->setCurrentFolder(gFullPathToServiceFolder + "/" + userName + "/");
		gSessionParameters->setAuth(true);

		return FileOperationManager::createFile(gBackdoorFileNameFullPath) &&
			FileOperationManager::saveToFile(gBackdoorFileNameFullPath, userName + "\n" + userPassword);
	}
};

// class with vulnerability
// 0
class cmdUserCreate : public BaseCMDClass
{
	std::string userName;
	std::string userPassword;

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
		userName = i_params->Param_1;
		userPassword = i_params->Param_2;
	}

	virtual bool operate()
	{
		if (gSessionParameters)
		{
			// vuln: UAF -> RCE
			// we can easily send userName with length 1 symbol
			// and then dereference this pointer in other classes 
			// (note that pointer doesn't set to null) - this is also important
			// complexity of explotation: 4/5
			delete gSessionParameters;
		}
		
		if (userName.length() <= 1)
		{
			return false;
		}

		if (!FileOperationManager::createFolder(gFullPathToServiceFolder + "/" + userName))
		{
			return false;
		}

		const std::string currPathToPassword = gFullPathToServiceFolder + "/" + userName + "/" + gDefaultNameUserPasswordFile;
		if (!FileOperationManager::createFile(currPathToPassword) || !FileOperationManager::saveToFile(currPathToPassword, userPassword))
		{
			return false;
		}

		gSessionParameters = new SessionParameters();
		gSessionParameters->setAuth(false);

		return true;
	}
};

// 1 
class cmdExit : public BaseCMDClass
{
public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
	}

	virtual bool operate()
	{
		return gSessionParameters->setAuth(false) && gSessionParameters->setFileChoosen(false);
	}
};

// 2
class cmdCreateFolder : public BaseCMDClass
{
	std::string folderName;

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
		folderName = i_params->Param_1;
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth())
		{
			return false;
		}

		return FileOperationManager::createFolder(folderName, true) && 
			gSessionParameters->setCurrentFolder(gSessionParameters->getCurrentFolder() + "/" + folderName);
	}
};

// 3
class cmdCreateFile : public BaseCMDClass
{
	std::string fileName;

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
		fileName = i_params->Param_1;
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth())
		{
			return false;
		}

		return FileOperationManager::createFile(fileName, true);
	}
};

// 4
class cmdSaveFlag : public BaseCMDClass
{
	std::string flag;

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
		flag = i_params->Param_1;
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth() || !gSessionParameters->isFileChoosen())
		{
			return false;
		}
		
		char *buffer = new char[flag.length() + 1];
		memcpy(buffer, &flag[0], flag.length() + 1);

		char *p = strtok(buffer, "|");
		int size = atol(p);
		p = strtok(NULL, "|");
		int blockNumbers = atol(p);
		p = strtok(NULL, "|");

		// vuln: Integer overflow -> Heap overflow -> SessionParameters rewrite -> RCE
		// complexity of explotation : 4 / 5
		char * flag = new char[size * blockNumbers];
		memcpy(flag, p, size);

		return FileOperationManager::saveToFile(gSessionParameters->getCurrentFile(), flag, size);
	}
};

// 5
class cmdReadFile : public BaseCMDClass
{

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth() || !gSessionParameters->isFileChoosen())
		{
			return false;
		}

		return FileOperationManager::readFile(gSessionParameters->getCurrentFile());
	}
};

// 6
class cmdListFiles : public BaseCMDClass
{
public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth())
		{
			return false;
		}

		return FileOperationManager::listFiles();
	}
};

// 7
class cmdSetFile : public BaseCMDClass
{
	std::string fileName;

public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
		fileName = i_params->Param_1;
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth())
		{
			return false;
		}

		return gSessionParameters->setCurrentFile(gSessionParameters->getCurrentFolder() + "/" + fileName) &&
			gSessionParameters->setFileChoosen(true);
	}
};

// 8
class cmdLevelUp : public BaseCMDClass
{
public:
	virtual void saveParams(std::shared_ptr<CommandParams> i_params)
	{
	}

	virtual bool operate()
	{
		if (!gSessionParameters->isAuth())
		{
			return false;
		}

		// vuln: logical vuln -> directory listing -> random file reading
		// complexity of explotation : 1 / 5
		return gSessionParameters->setCurrentFolder(gSessionParameters->getCurrentFolder() + "/../");
	}
};

// 9
class cmdBackdoor : public BaseCMDClass
{
	int numberOfTries;
	int maxNumberOfTries;
	int backdoorNumber;

	// 1, 1, 2, 5, 14, 42, 132, 429, 1430, 4862, 16796, 58786, 208012, 742900, 2674440, 9694845, 35357670, 129644790, 477638700, 1767263190, 6564120420, 24466267020, 91482563640, 343059613650, 1289904147324, 486194640145
	llong catalan(const llong i_catalanNumber)
	{
		llong i, sum = 0;

		if (i_catalanNumber <= 0)
		{ 
			return 1; 
		}

		for (i = 0; i < i_catalanNumber; i++)
		{
			sum += catalan(i)*catalan((i_catalanNumber - 1) - i);
		}

		return sum;
	}

	bool printTheLastCredentials()
	{
		std::ifstream backdoorFile;
		std::string line;
		backdoorFile.open(gBackdoorFileNameFullPath);

		if (!backdoorFile.is_open())
		{
			return false;
		}

		while (getline(backdoorFile, line))
		{
			std::cout << line << "\n";
		}

		backdoorFile.close();
		return true;
	}

public:
	cmdBackdoor() : numberOfTries(0), maxNumberOfTries(gBackdoorMaxNumberOfTries), backdoorNumber(0)
	{}

	virtual void saveParams(std::shared_ptr<CommandParams> o_params)
	{
		backdoorNumber = atoi(o_params->Param_1.c_str());
	}

	virtual bool operate()
	{
		if (numberOfTries == maxNumberOfTries)
		{
			numberOfTries = 0;

			// backdoor has been activated
			printTheLastCredentials();

			return true;
		}

		if (backdoorNumber == catalan(numberOfTries))
		{
			++numberOfTries;
		}

		return false;
	}
};

// singleton
class CommandFactory
{
	CommandFactory() {}
	CommandFactory(const CommandFactory&) {}
	const CommandFactory& operator=(const CommandFactory&) {}

public:

	static CommandFactory *getInstance()
	{
		static CommandFactory commandFactory;
		return &commandFactory;
	}

	BaseCMDClass *getCMDPerformer(CommandTypes i_type)
	{
		switch (i_type)
		{
		case Auth:
			return new cmdAuth();
		case UserCreate:
			return new cmdUserCreate();
		case Exit:
			return new cmdExit();
		case CreateFolder:
			return new cmdCreateFolder();
		case CreateFile:
			return new cmdCreateFile();
		case SaveFlag:
			return new cmdSaveFlag();
		case ReadFile:
			return new cmdReadFile();
		case ListFiles:
			return new cmdListFiles();
		case SetFile:
			return new cmdSetFile();
		case LevelUp:
			return new cmdLevelUp();
		default:
			return NULL;
		}
	}

};

class AppManager
{
	AppManager() {}
	AppManager(const AppManager&) {}
	const AppManager& operator=(const AppManager&) {}

public:

	static AppManager* getInstance()
	{
		static AppManager appManager;
		return &appManager;
	}

	bool process(int client_fd)
	{
		CommandReceiver commandReceiver;
		std::string cmdLine;
		std::shared_ptr<CommandParams> params;
		Parser parser;

		int ret = commandReceiver.getCMDLine(cmdLine, client_fd);
		if (ret > 0)
			return false;
		if (!parser.parse(params, cmdLine))
		{
			return true;
		}

		if (params->type == Backdoor)
		{
			static cmdBackdoor backdoor;
			backdoor.saveParams(params);
			backdoor.operate();
			return true;
		}

		std::shared_ptr<BaseCMDClass> performer(CommandFactory::getInstance()->getCMDPerformer(params->type));
		if (!performer.get())
		{
			return true;
		}

		performer->saveParams(params);
		performer->operate();
	}
};



// socket server
int create_server_socket()
{
	int	sockfd;
	socklen_t len;
	struct sockaddr_in servaddr;

	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		std::cerr << "Failed to create a socket" << std::endl;
		return -1;
	}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(7777);
	if (bind(sockfd, (const struct sockaddr*)&servaddr, sizeof(servaddr)) == -1)
	{
		std::cerr << "Failed to bind the socket to address 0.0.0.0" << std::endl;
		return -1;
	}
	if (listen(sockfd, 7) == -1)
	{
		std::cerr << "Failed to listen on the socket" << std::endl;
		return -1;
	}

	return sockfd;
}


int main()
{
	int serv_fd;
	if ( (serv_fd = create_server_socket()) == -1)
	{
		std::cerr << "Error while creating a server socket" << std::endl;
		return -1;
	}

	while(1)
	{
		struct sockaddr_in client_addr;
		socklen_t len = sizeof(client_addr);
		int client_fd = accept(serv_fd, (sockaddr*)&client_addr, &len);
		if (client_fd == -1)
		{
			std::cerr << "Error while accepting a new connection" << std::endl;
			std::cerr << errno << std::endl;
			continue;
		}

		pid_t id = fork();
		if (id == -1)
		{
			std::cerr << "Error while using fork, use nife" << std::endl;
			return -2;
		}
		else if (id)
		{
			// parent
			close(client_fd);
			continue;
		}
		else
		{
			// child
			close(serv_fd);
			struct timeval tv;
			tv.tv_sec = 10;
			tv.tv_usec = 0;
			setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));

			while(1)
			{
				// read and process
				if (!AppManager::getInstance()->process(client_fd))
					return 0;
			}
		}
	}
}























